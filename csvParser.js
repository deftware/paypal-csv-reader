const Papa = require('papaparse');
const fs = require('fs');
const curExchange = require('./currencyExchange');

let avalMonths = [];

function populateMonths(year) {
    avalMonths = [];
    fs.readdirSync(`./data/csv/${year}/`).forEach(file => {
        avalMonths.push(file.substring(0, file.length - ".csv".length).toString());
    });
    avalMonths.sort((a, b) => curExchange.allMonths.indexOf(a) - curExchange.allMonths.indexOf(b));
}

async function getMap(year) {
    let data = {};
    for (let month of avalMonths) {
        data[month] = {
            gross: {
                original: 0,
                sek: 0
            },
            net: {
                original: 0,
                sek: 0
            },
            fees: {
                original: 0,
                sek: 0
            },
            rate: (await curExchange.getExchangeRateLastDay(year, month, "EUR", "SEK")).toFixed(4)
        };
    }
    return data;
}

async function usdToEuro(amount, year, month) {
    return amount / (await curExchange.getExchangeRateLastDay(year, month, "EUR", "USD"));
}

/**
 * "Date","Time","TimeZone","Name","Type","Status","Currency","Gross","Fee","Net","From Email Address","To Email Address","Transaction ID","Shipping Address","Address Status","Item Title","Item ID","Shipping and Handling Amount","Insurance Amount","Sales Tax","Option 1 Name","Option 1 Value","Option 2 Name","Option 2 Value","Reference Txn ID","Invoice Number","Custom Number","Quantity","Receipt ID","Balance","Address Line 1","Address Line 2/District/Neighborhood","Town/City","State/Province/Region/County/Territory/Prefecture/Republic","Zip/Postal Code","Country","Contact Phone Number","Subject","Note","Country Code","Balance Impact"
 * @param {String} year 
 * @param {String} month 
 */
function readCSV(year, month) {
    return Papa.parse(fs.readFileSync(`./data/csv/${year}/${month}.csv`).toString(), {}).data;
}

/**
 * returns all payments made and all payments recivied in all months of a specified year. 
 * @param {String} year 
 */
async function getTransactions(year) {
    populateMonths(year);
    let data = {};
    for (let month of avalMonths) {
        data[month] = {
            "outgoing": [],
            "incomming": []
        };
        let csv = readCSV(year, month);
        let rate = (await curExchange.getExchangeRateLastDay(year, month, "EUR", "SEK")).toFixed(4);
        csv.shift();
        csv.pop();
        for (let tx of csv) {
            let transaction = {
                name: tx[3],
                type: tx[4],
                gross: parseFloat(tx[7].replace(",", "")),
                net: parseFloat(tx[9].replace(",", "")),
                fee: parseFloat(tx[8].replace(",", "")),
                txId: tx[12],
                title: tx[15],
                cur: tx[6],
                rate: rate,
                status: tx[5]
            };
            data[month][transaction.gross < 0 ? "outgoing" : "incomming"].push(transaction);
        }
    };
    return data;
}

/**
 * Returns the sum of a given year. Gross, net, and paypal transaction fees.
 * @param {String} year 
 */
async function getSum(year) {
    populateMonths(year);
    let data = await getMap(year);
    for (let month of avalMonths) {
        let transactions = (await getTransactions(year))[month]['incomming'];
        for (let transaction of transactions) {
            if (transaction.cur === "SEK") {
                // This can be ignored
            } else {
                // Convert to Euro first
                if (transaction.cur === "USD") {
                    transaction.gross = await usdToEuro(transaction.gross, year, month);
                    if (transaction.fee !== 0) {
                        transaction.fee = await usdToEuro(transaction.fee, year, month);
                    }
                    transaction.net = await usdToEuro(transaction.net, year, month);
                }
                // Gross
                data[month].gross.original += transaction.gross;
                data[month].gross.sek += Math.abs(transaction.gross) * data[month].rate;
                // Net
                data[month].net.original += Math.abs(transaction.net);
                data[month].net.sek += Math.abs(transaction.net) * data[month].rate;
                // Fees
                data[month].fees.original += Math.abs(transaction.fee);
                data[month].fees.sek += Math.abs(transaction.fee) * data[month].rate;
            }
        }
    }
    return data;
}

/**
 * Returns the payments for a given year
 * @param {String} year 
 */
async function getSentPayments(year) {
    populateMonths(year);
    let data = {};
    for (let month of avalMonths) {
        data[month] = {
            withdrawl: {
                SEK: 0,
                EUR: 0
            },
            USD: {
                gross: 0,
                rate: (await curExchange.getExchangeRateLastDay(year, month, "EUR", "USD")).toFixed(4)
            },
            EUR: {
                gross: 0,
                converted: 0,
                rate: (await curExchange.getExchangeRateLastDay(year, month, "EUR", "SEK")).toFixed(4)
            },
            SEK: {
                converted: 0
            }
        };
        let transactions = (await getTransactions(year))[month]['outgoing'];
        for (let transaction of transactions) {
            transaction.gross = Math.abs(transaction.gross);
            transaction.net = Math.abs(transaction.net);
            transaction.fee = Math.abs(transaction.fee);
            if (transaction.type === "Hold on Balance for Dispute Investigation" || transaction.type === "Chargeback" || transaction.type === "Payment Reversal" ||
                transaction.type === "Payment Refund" || transaction.type === "General Currency Conversion" || transaction.status === "Pending") {
                // Can be ignored
                continue;
            }
            if (transaction.type === "General Withdrawal") {
                data[month].withdrawl.SEK += transaction.gross;
                continue;
            }
            // Personal payments
            if (transaction.type === "PreApproved Payment Bill User Payment" && (transaction.cur === "SEK" && transaction.gross === 99|| transaction.cur === "USD" && transaction.gross === 4.99) ||
                transaction.name === "www.steampowered.com" || transaction.name === "G2A.COM Limited") {
                // Can be ignored
                continue;
            }
            if (transaction.cur === "SEK") {
                // Conver to Euro
                transaction.gross = transaction.gross / data[month].EUR.rate;
                data[month].EUR.converted += transaction.gross;
            }
            if (transaction.cur === "USD") {
                // Convert to Euro
                data[month].USD.gross += transaction.gross;
                transaction.gross = await usdToEuro(transaction.gross, year, month);
                if (transaction.fee !== 0) {
                    transaction.fee = await usdToEuro(transaction.fee, year, month);
                }
                transaction.net = await usdToEuro(transaction.net, year, month);
                data[month].EUR.converted += transaction.gross;
            }
            data[month].EUR.gross += transaction.gross
            data[month].SEK.converted += transaction.gross * data[month].EUR.rate;
        }
        data[month].withdrawl.EUR = data[month].withdrawl.SEK / data[month].EUR.rate;
    }
    return data;
}

module.exports = {
    populateMonths,
    readCSV,
    getSum,
    getTransactions,
    getSentPayments
};