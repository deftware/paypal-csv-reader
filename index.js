const fs = require('fs');

if (!fs.existsSync('./data/')) {
    fs.mkdirSync('./data/');
    fs.mkdirSync('./data/csv/');
    fs.writeFileSync('./data/config.json', JSON.stringify({
        fixerAccessToken: ""
    }));
    fs.writeFileSync('./data/rates.json', JSON.stringify({}));
    return console.warn("Please set \"fixerAccessToken\" in data/config.json before running the server and your csv files in data/csv/<year>/");
}