const fs = require('fs');
const fetch = require('node-fetch');

// Fixer.io get's it's exchange rates from the European Central Bank
const exchangeRateAPI = "http://data.fixer.io/api/";
const config = JSON.parse(fs.readFileSync("./data/config.json"));
const rates = JSON.parse(fs.readFileSync('./data/rates.json'));
const allMonths = ['january', 'febuary', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december'];

/**
 * Get's the exchange rate for 1 <currency> = x SEK, returns a float value with 6 decimals
 * @param {String} date 
 * @param {String} from The currency to convert from to SEK 
 */
async function getExchangeRate(date, from, to) {
    return new Promise((resolve, reject) => {
        if (rates[date] && rates[date][from][to]) {
            return resolve(rates[date][from][to]);
        }
        fetch(`${exchangeRateAPI}${date}?access_key=${config.fixerAccessToken}&base=${from}`).then(res => res.json()).then(json => {
            if (json.success) {
                if (!rates[date]) {
                    rates[date] = {};
                }
                if (!rates[date][from]) {
                    rates[date][from] = {};
                }
                rates[date][from][to] = json.rates[to];
                fs.writeFileSync('./data/rates.json', JSON.stringify(rates));
                resolve(json.rates[to]);
            } else {
                resolve("Failed to convert currency");
            }
        });
    });
}

/**
 * Get's the exchange rate of the last of a given month
 * @param {String} year 
 * @param {String} month 
 * @param {String} from The currency to convert from to SEK  
 */
function getExchangeRateLastDay(year, month, from, to) {
    month = allMonths.indexOf(month) + 1;
    return getExchangeRate(`${year}-${month < 10 ? "0" + month : month}-${getDaysInMonth(month, parseInt(year))}`, from, to);
}

function getDaysInMonth(m, y) {
    return m === 2 ? y & 3 || !(y % 25) && y & 15 ? 28 : 29 : 30 + (m + (m >> 3) & 1);
}

module.exports = {
    getExchangeRate,
    getExchangeRateLastDay,
    allMonths
};