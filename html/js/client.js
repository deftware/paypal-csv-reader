let euro = false;
let getSum, getSent;

async function readAddress(url) {
    return new Promise((resolve, reject) => {
        let request = new XMLHttpRequest();
        request.open('GET', url, true);
        request.onload = function () {
            if (request.status >= 200 && request.status < 400) {
                resolve(request.responseText);
            }
        };
        request.onerror = function () {
            reject();
        };
        request.send();
    });
}

async function loadTotal(euro) {
    let table = document.getElementById("tSum");
    table.innerHTML = "";
    Object.keys(getSum).forEach(month => {
        let newRow = table.insertRow(-1);
        let data = getSum[month];
        let index = 0;
        addCellToRow(newRow, month.substring(0, 1).toUpperCase() + month.substring(1), index);
        addCellToRow(newRow, euro ? "€" + data.gross.original.toFixed(2) : data.gross.sek.toFixed(2) + " SEK", ++index);
        addCellToRow(newRow, euro ? "€" + (data.net.original - getSent[month].EUR.gross).toFixed(2) : (data.net.sek - getSent[month].SEK.converted).toFixed(2) + " SEK", ++index);
        addCellToRow(newRow, euro ? "€" + data.fees.original.toFixed(2) : data.fees.sek.toFixed(2) + " SEK", ++index);
        addCellToRow(newRow, "€1 = " + data.rate + " SEK", ++index);
    });
}

async function loadSent(euro) {
    let table = document.getElementById("tSent");
    table.innerHTML = "";
    Object.keys(getSent).forEach(month => {
        let newRow = table.insertRow(-1);
        let data = getSent[month], sumData = getSum[month];
        let index = 0;
        addCellToRow(newRow, month.substring(0, 1).toUpperCase() + month.substring(1), index);
        addCellToRow(newRow, euro ? "€" + data.EUR.gross.toFixed(2) : data.SEK.converted.toFixed(2) + " SEK", ++index);
        addCellToRow(newRow, euro ? "€" + data.withdrawl.EUR.toFixed(2) : data.withdrawl.SEK.toFixed(2) + " SEK", ++index);
        addCellToRow(newRow, euro ? "€" + sumData.fees.original.toFixed(2) : sumData.fees.sek.toFixed(2) + " SEK", ++index);
        let total = 0;
        total += euro ? data.EUR.gross : data.SEK.converted;
        //total += euro ? data.withdrawl.EUR : data.withdrawl.SEK;
        total += euro ? sumData.fees.original : sumData.fees.sek;
        addCellToRow(newRow, euro ? "€" + total.toFixed(2) : total.toFixed(2) + " SEK", ++index);
    });
}

function addCellToRow(newRow, text, index) {
    newRow.className = "row100 body";
    let newCell = newRow.insertCell(index);
    newCell.className = "cell100 column" + (index + 1);
    newCell.appendChild(document.createTextNode(text));
}

async function bootstrap(euro) {
    getSum = JSON.parse(await readAddress('/getSum'));
    getSent = JSON.parse(await readAddress('/getSentPayments'));
    await loadSent(euro);
    await loadTotal(euro);
}

function switchCurrency() {
    euro = !euro;
    bootstrap(euro);
}

bootstrap(false);