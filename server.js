const fs = require('fs');
const csvParser = require('./csvParser');
const https = require('https');

const app = require('express')();

const cert = {
    key: fs.readFileSync('./cert/server.key'),
    cert: fs.readFileSync('./cert/server.crt')
} 

app.get('/', (req,res) => {
    res.sendFile(__dirname + "/html/index.html");
});

app.get('/getSentPayments', async (req,res) => {
    let data = await csvParser.getSentPayments("2019");
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(data));
});

app.get('/getSum', async (req,res) => {
    let data = await csvParser.getSum("2019");
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(data));
});

app.get('*', (req,res) => {
    res.sendFile(`${__dirname}/html/${req.url}`);
});

https.createServer(cert, app).listen(2087);